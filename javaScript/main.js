import {
    clients1, clients2, characters,
    user1, satoshi2018, satoshi2019,
    satoshi2020, books, bookToAdd,
    employee, array,
} from '/javaScript/dataBase.js';


const mergedFirstVersion = [...new Set([...clients1, ...clients2])];

const mergedSecondVersion = [...clients1, ...clients2].reduce((acc, client) => {
    if (!acc.includes(client)) acc.push(client);
    return acc;
}, []);

console.log(mergedFirstVersion, mergedSecondVersion);



const charactersShortInfo = characters.map(({ name, lastName, age }) => ({name, lastName, age}));

console.dir(charactersShortInfo);



const { name: forename, years: age, isAdmin: isAdmin = false } = user1;

console.log(forename, age, isAdmin);



const fullProfile = { ...satoshi2018, ...satoshi2019, ...satoshi2020 };

console.dir(fullProfile);



const newBookList = [...books, bookToAdd];

console.dir(newBookList);



const newEmployee = {
    ...employee,
    age: 30,
    salary: 5000
};

console.dir(newEmployee);



const [ value, showValue ] = array;

alert(value);
alert(showValue());